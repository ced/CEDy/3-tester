**Rappel** : Bien que la calculette de l'Ademe présente une interface graphique (GUI : Graphic User Interface), nous allons dans un premier temps, nous concentrer sur un programme en ligne de commandes (CLI : Command Line Interface).
**Écrire un programme CLI**, par rapport à un un programme GUI, présente de nombreux avantages, ne serait-ce que l'économie de moyens nécessaires au fonctionnement.
Un programme CLI sera surtout **beaucoup plus facile à écrire** dans un premier temps car nous n'aurons pas à interagir avec le serveur graphique et le gestionnaire de fenêtres et autres composants graphiques.

## Notre mission

Lors de nos missions précédentes, nous sommes parvenus à afficher notre
calculette et à demander à l'utilisateur de saisir la distance entre son
domicile et son travail ainsi que son mode de déplacement...
Mais nous n’avons pas pris en compte le mode de déplacement saisie,
nous avons considéré, choix raisonnable, que par défaut il prenait le train !
En effet, pour cela il nous manquait des connaissances :
la possibilité d'effectuer un test entre deux valeurs.
