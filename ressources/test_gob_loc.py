def modifier_entier(param):
    """
    Incrémente et affiche une valeur transmise en paramètre.

    param : entier à incrémenter

    Pas de valeur de retour
    """
    param = param + 1
    print("Nouvelle valeur :\t{}, référence :\t{}".format(param, id(param)))


def modifier_liste(liste):
    """
    Modifie et affiche une liste transmise en paramètre.

    liste : liste à modifier

    Pas de valeur de retour
    """
    liste[0] = 0
    print("Nouvelle liste\t:\t{}, référence :\t{}".format(liste, id(liste)))


# programme principale
nbr = 1
print("Valeur initiale :\t{}, référence :\t{}".format(nbr, id(nbr)))
modifier_entier(nbr)
print("Valeur finale\t:\t{}, référence :\t{}".format(nbr, id(nbr)))

l_nbr = [1, 2, 3]
print("Valeur initiale :\t{}, référence :\t{}".format(l_nbr, id(l_nbr)))
modifier_liste(l_nbr)
print("Valeur finale\t:\t{}, référence :\t{}".format(l_nbr, id(l_nbr)))
