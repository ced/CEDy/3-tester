La modification de variables passées en argument à des fonctions
n'a pas toujours le même effet en python selon le type de la variable.

Pour être plus précis, le passage par valeur ou référence se fera
selon que le type est immuable ou mutable .
