def affiche_cout(tab, nb):
    """
    Affichage des différents éléments d'un tableau, séparés par une tabulation.

    tab : liste des éléments
    nb : entier représentant le nombre de tabulations à afficher

    pas de valeur de retour
    """
    for e in tab:
        print("\t", e, end="\t"*nb)
    print()


def calcule_cout_km(les_couts, km):
    """
    Calcule les coûts de déplacement.

    les_couts : liste des coûts pour 1 km d'un mode de déplacement
    km : distance entre le domicile et le lieu de travail

    retourne une liste contenant les coûts kilométriques
    pour les kms passés en paramètre
    """
    res = [0, 0]   # Initialiser la liste des résultats
    indice = 0
    for un_cout in les_couts:
        res[indice] = round(un_cout * km, 1)
        indice = indice + 1
    return res


def choix_entier(message):
    """
    Demande à l'utilisateur de saisir un entier.

    message : question posée à l'utilisateur

    retourne un entier
    """
    return int(input(message))


def choix_deplacement(menu, km, cout_train, cout_velo, cout_voiture, dep):
    """
    Demande à l'utilisateur de saisir son mode de déplacement

    menu : chaîne de caractères présentant les différents mode de déplacement
    km : distance entre le domicile et le lieu de travail (entier)
    cout_train : listes de coûts kilométriques pour un déplacement en train
    cout_velo : listes de coûts kilométriques pour un déplacement en vélo
    cout_voiture : listes de coûts kilométriques pour un déplacement en voiture
    dep : chaîne représentant l'itération sur le mode de déplacement

    retourne une liste des coûts kilométriques liés à un mode de déplacement
    """
    val_dep = choix_entier(menu.format(dep))
    if val_dep == 1:
        return calcule_cout_km(cout_train, km)
    elif val_dep == 2:
        return calcule_cout_km(cout_velo, km)
    elif val_dep == 3:
        return calcule_cout_km(cout_voiture, km)
    elif val_dep == 4:
        exit(0)


# Définition des variables globales
en_tete = """\
Calculette : Eco-déplacements.
Calculez l'impact de vos déplacements quotidiens
sur l'environnement et vos dépenses"""
bilan = "Bilan annuel des coûts pour ce mode de déplacement :"
en_tete_cout = ["kg eq. CO2", "l eq. pétrole"]
ligne = "#" * 50
distance = """\
Quelle est la distance entre le domicile et le lieu de travail ? """
deplacement = """\
Choisir le {} mode de déplacement :
1 - train
2 - vélo
3 - voiture
4 - quitter
Mon choix : """
train = [14.62, 9.26]
velo = [0, 0]
voiture = [129.62, 50.65]

# programme principal
print(ligne)
print(en_tete)
print(ligne)
km = choix_entier(distance)
mes_couts = choix_deplacement(deplacement, km, train, velo, voiture,
                              "premier")
print("\n", ligne, sep='')
print(bilan)
affiche_cout(en_tete_cout, 1)
affiche_cout(mes_couts, 2)
